#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_f2q.mk

COMMON_LUNCH_CHOICES := \
    lineage_f2q-user \
    lineage_f2q-userdebug \
    lineage_f2q-eng
