#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from f2q device
$(call inherit-product, device/samsung/f2q/device.mk)

PRODUCT_DEVICE := f2q
PRODUCT_NAME := lineage_f2q
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-F916U
PRODUCT_MANUFACTURER := samsung

PRODUCT_GMS_CLIENTID_BASE := android-samsung-ss

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="f2qsqw-user 13 TP1A.220624.014 F916USQS2JWC1 release-keys"

BUILD_FINGERPRINT := samsung/f2qsqw/f2q:11/RP1A.200720.012/F916USQS2JWC1:user/release-keys
