#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_f2q.mk

COMMON_LUNCH_CHOICES := \
    omni_f2q-user \
    omni_f2q-userdebug \
    omni_f2q-eng
